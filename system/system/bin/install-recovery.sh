#!/system/bin/sh
if ! applypatch --check EMMC:/dev/block/bootdevice/by-name/recovery:67108864:b0298ac70d7d8ee32e36f22174ac62f7168766a0 > /cache/recovery/last_install_recovery_status; then
  applypatch  \
          --patch /system/recovery-from-boot.p \
          --source EMMC:/dev/block/bootdevice/by-name/boot:33554432:a3bcf3c756aa7cd7d629337d99c3be62abb7a464 \
          --target EMMC:/dev/block/bootdevice/by-name/recovery:67108864:b0298ac70d7d8ee32e36f22174ac62f7168766a0 >> /cache/recovery/last_install_recovery_status && \
      echo "Installing new recovery image: succeeded" >> /cache/recovery/last_install_recovery_status || \
      echo "Installing new recovery image: failed" >> /cache/recovery/last_install_recovery_status
else
  echo "Recovery image already installed" >> /cache/recovery/last_install_recovery_status
fi
