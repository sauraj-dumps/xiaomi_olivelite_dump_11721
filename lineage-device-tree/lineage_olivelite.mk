#
# Copyright (C) 2023 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

# Inherit from those products. Most specific first.
$(call inherit-product, $(SRC_TARGET_DIR)/product/full_base_telephony.mk)

# Inherit some common Lineage stuff.
$(call inherit-product, vendor/lineage/config/common_full_phone.mk)

# Inherit from olivelite device
$(call inherit-product, device/xiaomi/olivelite/device.mk)

PRODUCT_DEVICE := olivelite
PRODUCT_NAME := lineage_olivelite
PRODUCT_BRAND := Xiaomi
PRODUCT_MODEL := Redmi 8A
PRODUCT_MANUFACTURER := xiaomi

PRODUCT_GMS_CLIENTID_BASE := android-xiaomi

PRODUCT_BUILD_PROP_OVERRIDES += \
    PRIVATE_BUILD_DESC="olivelite-user 10 QKQ1.191014.001 V12.5.2.0.QCPRUXM release-keys"

BUILD_FINGERPRINT := Xiaomi/olivelite/olivelite:10/QKQ1.191014.001/V12.5.2.0.QCPRUXM:user/release-keys
