#
# Copyright (C) 2023 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

# Health
PRODUCT_PACKAGES += \
    android.hardware.health@2.1-impl \
    android.hardware.health@2.1-impl.recovery \
    android.hardware.health@2.1-service

# Overlays
PRODUCT_ENFORCE_RRO_TARGETS := *

# Product characteristics
PRODUCT_CHARACTERISTICS := default

# Rootdir
PRODUCT_PACKAGES += \
    init.qcom.early_boot.sh \
    init.qti.ims.sh \
    init.qti.fm.sh \
    init.class_main.sh \
    init.qcom.coex.sh \
    init.qti.qseecomd.sh \
    init.qcom.sh \
    init.qcom.crashdata.sh \
    init.qcom.post_boot.sh \
    init.qcom.sensors.sh \
    init.qcom.efs.sync.sh \
    init.qcom.usb.sh \
    init.qcom.class_core.sh \
    init.qcom.sdio.sh \
    init.qti.can.sh \
    init.crda.sh \
    qca6234-service.sh \
    init.mdm.sh \

PRODUCT_PACKAGES += \
    fstab.qcom \
    init.target.rc \
    init.msm.usb.configfs.rc \
    init.qcom.usb.rc \
    init.qcom.rc \
    init.qcom.factory.rc \

# Shipping API level
PRODUCT_SHIPPING_API_LEVEL := 28

# Soong namespaces
PRODUCT_SOONG_NAMESPACES += \
    $(LOCAL_PATH)

# Inherit the proprietary files
$(call inherit-product, vendor/xiaomi/olivelite/olivelite-vendor.mk)
